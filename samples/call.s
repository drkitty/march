    clr C
    imm (_r - _a)
    gi4 R0
_a:
    add R0
    add R1
    add R2
    add R3
    psh R3
    psh R2
    psh R1
    psh R0
    brx foo
_r:

foo:
    pop R0
    pop R1
    pop R2
    pop R3
    pi4 R0
