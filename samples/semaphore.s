    ### binary semaphore ###

down:
    lk2 RE

up:
    imm 1
    st2 RE

    ### semaphore ###

down:
    lk2 RE
_spin:
    ld2 RC
    brx Z _spin
    put R0
    dec R0
    get R0
    st2 RC
    imm 1
    st2 RE

up:
    lk2 RE
    ld2 RC
    put R0
    inc R0
    get R0
    st2 RC
    imm 1
    st2 RE
