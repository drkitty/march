    # arguments are in {R1,R0} and {R3,R2}.

    # negate second argument
    not R2
    not R3
    inc R2
    imm 0
    add R3

    # add arguments
    clr C
    get R2
    add R0
    imm 0
    get R3
    add R1

    # return value is in {R1,R0}
